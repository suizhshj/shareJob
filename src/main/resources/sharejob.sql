/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : sharejob

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2018-06-18 22:44:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL COMMENT '用户ID',
  `rec_id` int(10) DEFAULT NULL COMMENT '招聘信息ID',
  `content` varchar(250) DEFAULT NULL COMMENT '评论内容',
  `create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
  `last_update_datetime` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('2', null, null, null, '2018-06-17 00:51:15', '2018-06-17 00:51:15');
INSERT INTO `comment` VALUES ('3', null, null, null, '2018-06-17 00:51:44', '2018-06-17 00:51:44');
INSERT INTO `comment` VALUES ('4', null, null, '', '2018-06-17 00:51:50', '2018-06-17 00:51:50');
INSERT INTO `comment` VALUES ('5', null, '29', 'gggggggggg强制性羞耻视频', '2018-06-17 00:51:58', '2018-06-17 00:51:58');
INSERT INTO `comment` VALUES ('6', null, null, '三生三世十里桃花', '2018-06-17 16:01:13', '2018-06-17 16:01:13');
INSERT INTO `comment` VALUES ('7', null, '29', '三生三世十里桃花', '2018-06-17 16:14:02', '2018-06-17 16:14:02');
INSERT INTO `comment` VALUES ('8', null, '29', '人人入人人爱', '2018-06-17 16:16:23', '2018-06-17 16:16:23');
INSERT INTO `comment` VALUES ('9', null, '29', '人人入人人爱', '2018-06-17 16:16:25', '2018-06-17 16:16:25');
INSERT INTO `comment` VALUES ('10', null, '29', '人人入人人爱', '2018-06-17 16:16:26', '2018-06-17 16:16:26');
INSERT INTO `comment` VALUES ('11', null, '29', '111111', '2018-06-17 00:49:38', '2018-06-17 00:49:38');
INSERT INTO `comment` VALUES ('12', null, '29', '人人入人人爱', '2018-06-17 16:16:29', '2018-06-17 16:16:29');
INSERT INTO `comment` VALUES ('13', null, '29', '人人入人人爱', '2018-06-17 16:16:29', '2018-06-17 16:16:29');
INSERT INTO `comment` VALUES ('14', null, '29', '人人入人人爱', '2018-06-17 16:16:29', '2018-06-17 16:16:29');
INSERT INTO `comment` VALUES ('15', null, '29', '人人入人人爱', '2018-06-17 16:16:30', '2018-06-17 16:16:30');
INSERT INTO `comment` VALUES ('16', null, '29', '人人入人人爱', '2018-06-17 16:16:30', '2018-06-17 16:16:30');
INSERT INTO `comment` VALUES ('17', null, '29', '人人入人人爱', '2018-06-17 16:16:30', '2018-06-17 16:16:30');
INSERT INTO `comment` VALUES ('18', null, '29', '人人入人人爱', '2018-06-17 16:16:31', '2018-06-17 16:16:31');
INSERT INTO `comment` VALUES ('19', null, '29', '人人入人人爱', '2018-06-17 16:16:36', '2018-06-17 16:16:36');
INSERT INTO `comment` VALUES ('20', null, '29', '人人入人人爱', '2018-06-17 16:16:37', '2018-06-17 16:16:37');
INSERT INTO `comment` VALUES ('21', null, '29', '人人入人人爱', '2018-06-17 16:17:45', '2018-06-17 16:17:45');
INSERT INTO `comment` VALUES ('22', null, '29', '征战诸天世界', '2018-06-17 16:17:58', '2018-06-17 16:17:58');
INSERT INTO `comment` VALUES ('23', null, '29', '征战诸天世界hhgfff', '2018-06-17 16:18:12', '2018-06-17 16:18:12');
INSERT INTO `comment` VALUES ('24', null, '29', 'ppp项目是什么意思', '2018-06-17 16:18:23', '2018-06-17 16:18:23');
INSERT INTO `comment` VALUES ('25', null, '29', 'ppp项目是什么意思', '2018-06-17 16:22:46', '2018-06-17 16:22:46');
INSERT INTO `comment` VALUES ('26', null, '29', 'ppp项目是什么意思', '2018-06-17 16:22:56', '2018-06-17 16:22:56');
INSERT INTO `comment` VALUES ('27', null, '29', 'ppp项目是什么意思', '2018-06-17 16:23:07', '2018-06-17 16:23:07');
INSERT INTO `comment` VALUES ('28', null, '12', '头疼图片', '2018-06-17 16:23:24', '2018-06-17 16:23:24');
INSERT INTO `comment` VALUES ('29', null, '27', '不错不错', '2018-06-17 16:23:41', '2018-06-17 16:23:41');
INSERT INTO `comment` VALUES ('30', null, '29', 'baby是什么意思', '2018-06-17 16:26:42', '2018-06-17 16:26:42');
INSERT INTO `comment` VALUES ('31', null, '29', '海草舞', '2018-06-17 16:29:00', '2018-06-17 16:29:00');
INSERT INTO `comment` VALUES ('32', null, '29', '海草舞', '2018-06-17 16:29:34', '2018-06-17 16:29:34');
INSERT INTO `comment` VALUES ('33', null, '29', '海草舞', '2018-06-17 16:29:35', '2018-06-17 16:29:35');
INSERT INTO `comment` VALUES ('34', null, '29', '海草舞', '2018-06-17 16:29:37', '2018-06-17 16:29:37');
INSERT INTO `comment` VALUES ('35', null, '29', '北京天气', '2018-06-17 16:29:47', '2018-06-17 16:29:47');
INSERT INTO `comment` VALUES ('36', null, '29', '拉卡拉收款宝', '2018-06-17 16:30:45', '2018-06-17 16:30:45');

-- ----------------------------
-- Table structure for recruitment
-- ----------------------------
DROP TABLE IF EXISTS `recruitment`;
CREATE TABLE `recruitment` (
  `rid` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `company_name` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `area` varchar(20) DEFAULT NULL,
  `company_address` varchar(200) DEFAULT NULL COMMENT '公司地址',
  `job_title` varchar(200) DEFAULT NULL COMMENT '标题',
  `html` varchar(4000) DEFAULT NULL,
  `description` varchar(3200) DEFAULT NULL COMMENT '其它描述',
  `status` varchar(10) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `last_update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recruitment
-- ----------------------------
INSERT INTO `recruitment` VALUES ('12', '大三得电子科技有限公司', '和平', '福永和平福园一路高新建工业园', '大三得电子科技有限公司招聘作业员', '<p>\n	范德萨奋斗\n</p>\n<p>\n	fdsfdsfssdfsadfda\n</p>', '范德萨奋斗\n\n\n	fdsfdsfssdfsadfda', '··········', '2018-06-12 00:00:00', '2018-06-12 18:24:33', '2018-06-14 18:26:01');
INSERT INTO `recruitment` VALUES ('27', '大三的电子公司', '福永和平', '福永和平福缘一路', '大三的招聘作业员', '<p>\n	<strong>范德萨范德萨</strong> \n</p>\n<p>\n	范德萨范德萨富士达\n</p>\n<p>\n	fdsafdsa范德萨范德萨分\n</p>\n<p>\n	<strong>范德萨范德萨发送</strong> \n</p>', '范德萨范德萨 \n\n\n	范德萨范德萨富士达\n\n\n	fdsafdsa范德萨范德萨分\n\n\n	范德萨范德萨发送', '有效', '2018-06-14 00:00:00', '2018-06-14 17:53:00', '2018-06-14 18:25:58');
INSERT INTO `recruitment` VALUES ('28', 'AAA', 'CCC', 'VVV', '1111', '<p>\n	FSDFDS\n</p>\n<p>\n	FDSAFD\n</p>\n<p>\n	SAFDSA\n</p>\n<p>\n	<span style=\"background-color:#E53333;\"><strong>FDSA</strong></span>\n</p>\n<p>\n	<span style=\"background-color:#E53333;\"><strong>F</strong></span>\n</p>\n<p>\n	<span style=\"background-color:#E53333;\"><strong>DSAFDSFDSAF</strong></span><span style=\"background-color:#E53333;\"><strong></strong></span>\n</p>', 'FSDFDS\n\n\n	FDSAFD\n\n\n	SAFDSA\n\n\n	FDSA\n\n\n	F\n\n\n	DSAFDSFDSAF', '有效', '2018-06-14 00:00:00', '2018-06-14 18:20:15', '2018-06-14 18:20:15');
INSERT INTO `recruitment` VALUES ('29', 'AAAA', 'QQQ', 'CCCC', '呜呜呜呜', '<p>\n	丰富的萨附加费克里斯大家f附近的隆盛科技范德萨\n</p>\n<p>\n	今飞凯达司法所打两份\n</p>\n<p>\n	附近的开始啦激发&nbsp;\n</p>\n<p>\n	解放开绿灯撒技法上来看\n</p>', '丰富的萨附加费克里斯大家f附近的隆盛科技范德萨\n\n\n	今飞凯达司法所打两份\n\n\n	附近的开始啦激发 \n\n\n	解放开绿灯撒技法上来看', '有效', '2018-06-16 00:00:00', '2018-06-16 23:19:06', '2018-06-16 23:19:06');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(10) DEFAULT NULL COMMENT '主键ID',
  `name` varchar(40) DEFAULT NULL COMMENT '名称',
  `resource_type` varchar(10) DEFAULT NULL COMMENT '资源类型，[menu|button]',
  `url` varchar(255) DEFAULT NULL COMMENT '资源路径.',
  `permission` varchar(40) DEFAULT NULL COMMENT '权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view',
  `parent_id` int(10) DEFAULT NULL COMMENT '父编号',
  `parent_ids` varchar(255) DEFAULT NULL COMMENT '父编号列表',
  `sort` int(2) DEFAULT NULL COMMENT '排序',
  `create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
  `last_update_datetime` datetime DEFAULT NULL COMMENT '最后更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', '招聘管理', 'menu', '#', null, '0', '0|1', '1', '2018-06-08 00:00:00', '2018-06-08 00:00:00');
INSERT INTO `sys_permission` VALUES ('2', '招聘列表', 'menu', '/sharejob/recruitmentMain', 'recruitment:list', '1', '0|1', '1', '2018-06-08 00:00:00', '2018-06-08 00:00:00');

-- ----------------------------
-- Table structure for sys_permission_role_ref
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_role_ref`;
CREATE TABLE `sys_permission_role_ref` (
  `id` int(10) DEFAULT NULL,
  `permission_id` int(10) DEFAULT NULL COMMENT '权限ID',
  `role_id` int(10) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission_role_ref
-- ----------------------------
INSERT INTO `sys_permission_role_ref` VALUES ('1', '1', '1');
INSERT INTO `sys_permission_role_ref` VALUES ('2', '2', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(10) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描叙',
  `available` tinyint(1) DEFAULT NULL COMMENT '是否启用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', '管理员', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '登录密码',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `create_datetime` datetime DEFAULT NULL,
  `last_update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'suizhshj', '25eaf190465decfcb414161d782e7e7d', '眭善峰', '2018-06-08 00:00:00', '2018-06-08 00:00:00');

-- ----------------------------
-- Table structure for sys_user_role_ref
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_ref`;
CREATE TABLE `sys_user_role_ref` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sysuser_id` int(10) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role_ref
-- ----------------------------
INSERT INTO `sys_user_role_ref` VALUES ('1', '1', '1');
