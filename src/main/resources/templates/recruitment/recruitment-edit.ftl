<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>网站后台管理模版</title>
		<link rel="stylesheet" type="text/css" href="/static/lib/layui/css/layui.css"/>	
		<link rel="stylesheet" href="/static/kindeditor/themes/default/default.css" />	
	</head>
	<body>
		<div class="wrap-container">
			<form class="layui-form" style="width: 90%;padding-top: 20px;">
					
					<div class="layui-form-item">
						<label class="layui-form-label">公司名称：</label>
						<div class="layui-input-block">
							<input type="text" name="companyName" value="${recruitment.companyName!""}"  lay-verify="required" placeholder="请输入公司名称" autocomplete="off" class="layui-input">
						</div>

					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">公司地址：</label>
						<div class="layui-input-block">
					    	<input type="hidden"  name="rid" value="${recruitment.rid}"/>
							<input type="text" name="companyAddress" value="${recruitment.companyAddress!""}" lay-verify="required"  placeholder="请输入公司地址" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">所属区域：</label>
						<div class="layui-input-block">
							<input type="text" name="area" value="${recruitment.area!""}" lay-verify="required" placeholder="请输入所属区域" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">标题：</label>
						<div class="layui-input-block">
							<input type="text" name="jobTitle" value="${recruitment.jobTitle!""}" lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">有效日期：</label>
						<div class="layui-input-block">
							<input type="text" class="layui-input" value="${recruitment.effectiveDate?string('yyyy-MM-dd HH:mm:ss')}" lay-verify="required" id="effectiveDate" name="effectiveDate" placeholder="yyyy-MM-dd" readonly="readonly">							
						</div>
					</div>
					
					<div class="layui-form-item layui-form-text">
						<label class="layui-form-label">具体描述：</label>
						<div class="layui-input-block">
							<textarea name="description" id="description" placeholder="请输入内容" class="layui-textarea" style="height:300px;visibility:hidden;">${recruitment.description!""}</textarea>
						</div>
					</div>

					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn layui-btn-normal" lay-submit lay-filter="save">立即提交</button>
							<button type="reset" class="layui-btn layui-btn-primary">重置</button>
						</div>
					</div>
				</form>
		</div>
  		<script type="text/javascript" src="/static/js/jquery-3.2.1.js"></script>
		<script src="/static/lib/layui/layui.js" type="text/javascript" charset="utf-8"></script>
		<script src="/static/kindeditor/kindeditor-all-min.js" charset="utf-8" ></script>
		<script  src="/static/kindeditor/lang/zh-CN.js" charset="utf-8"></script>
		<script>
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="description"]', {
					allowFileManager : true
				});
				editor.html($('#description').val());
			});
			layui.use(['form','layedit','laydate'], function(){
			  var form = layui.form,
			      layer = layui.layer,
			      layedit = layui.layedit,
				  laydate = layui.laydate;	
				   //常规用法
				  laydate.render({
				    elem: '#effectiveDate',
				    type:'datetime'
				  });		  
				  //监听提交
				  form.on('submit(save)', function(data){
				  	 	
				  	 	if(editor.isEmpty()){
				  	 		layer.msg("描述不可以为空!",{time:1500});
				  	 		return false;
				  	 	}
						
				  	 	
				  	 	data.field.description = editor.text();
				  	 	data.field.html = editor.html();
					   $.ajax({  
						          type : "post",  
						          url :  "/recruitment/modify",  		         					    
						          data : data.field,		         
						          success : function(data){ 						          		         	
						          		layer.msg(data.msg);	
							        	  if(data.code =="SUCCESS"){
							        	  	 setTimeout(function(){			        	  	 	   
							        	  	 	  parent.closeAll();        	  	 	
							        	  	 },1000);							        	  	 	        			  	    	
						        	 	}
						          },						         
						    });
				    return false;
				  });
			});	
		</script>
	</body>

</html>