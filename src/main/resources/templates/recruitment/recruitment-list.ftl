<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title>欢迎页面-X-admin2.0</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="/static/css/font.css">
		<link rel="stylesheet" href="/static/css/xadmin.css">
		<script type="text/javascript" src="/static/js/jquery-3.2.1.js"></script>
		<script type="text/javascript" src="/static/lib/layui/layui.js" charset="utf-8"></script>
	</head>

	<body>
		<div class="x-nav">
			<span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">招聘信息管理</a>
        <a>
          <cite>招聘信息列表</cite></a>
      </span>
			<a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
				<i class="layui-icon" style="line-height:30px">ဂ</i></a>
		</div>
		<div class="x-body">
			<div class="layui-row">
				<form class="layui-form layui-col-md12 x-so">

					<div class="layui-form-item">
						<div class="layui-input-inline">
							<input type="text" id="companyName" name="companyName" placeholder="公司名称" class="layui-input">
						</div>
						<div class="layui-input-inline">
							<input type="text" id="area" name="area" placeholder="所在区域" class="layui-input">
						</div>
						<div class="layui-input-inline">
							<select id="status" name="status">
								<option value="">选择状态</option>
								<option value="有效">有效</option>
								<option value="失效">失效</option>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-inline">
							<input class="layui-input" placeholder="创建起始日" name="startDate" id="startDate">
						</div>
						<div class="layui-input-inline">
							<input class="layui-input" placeholder="创建截止日" name="endDate" id="endDate">
						</div>
						<div class="layui-input-inline">
							<button class="layui-btn" data-type="reload" lay-submit="" lay-filter="search" id="search"><i class="layui-icon">&#xe615;</i></button>
						</div>
					</div>	
				</form>
			</div>

			<div class="layui-btn-group">
				<button class="layui-btn layui-btn-sm btn-add">添加</button>&nbsp;
				<button class="layui-btn layui-btn-sm btn-del-batch">批量删除</button>
			</div>

			<table class="layui-hide" id="tableRec" lay-filter="rec"></table>
	</body>
	<script>
		layui.use(['form', 'jquery', 'laydate', 'element', "table"], function() {
			var
				form = layui.form,
				layer = layui.layer,
				laydate = layui.laydate,
				element = layui.element,
				$ = layui.jquery,
				table = layui.table;
			element.render();
			form.render();

			//日期范围	
			var endDate = laydate.render({
				elem: '#endDate', //选择器结束时间
				min: "1970-1-1", //设置min默认最小值
				done: function(value, date) {
					startDate.config.max = {
						year: date.year,
						month: date.month - 1, //关键
						date: date.date,
						hours: 0,
						minutes: 0,
						seconds: 0
					}
				}
			});

			var startDate = laydate.render({
				elem: '#startDate',
				max: "2099-12-31", //设置一个默认最大值
				done: function(value, date) {
					endDate.config.min = {
						year: date.year,
						month: date.month - 1, //关键
						date: date.date,
						hours: 0,
						minutes: 0,
						seconds: 0
					};
				}
			});

			$('.btn-add').on('click', function() {
				layer.open({
					title: '招聘信息添加',
					type: 2,
					content: '/sharejob/recruitmentAdd',
					area: ['80%', '90%']
				});
			});

			//方法级渲染
			table.render({
				elem: '#tableRec',
				url: '/recruitment/getGrid',
				method: 'post',
				limits: [5, 10, 15, 20, 30],
				limit: 10, //默认采用60
				cols: [
					[{
						type: 'checkbox',
						fixed: 'left',						
					}, {
						field: 'companyName',
						title: '公司名称',
						align: 'center',
						width: 180,
						fixed: 'left',
					}, {
						field: 'area',
						title: '所属区域',
						sort: true,
						width: 100,
						align: 'center',
					},{
						field: 'companyAddress',
						title: '公司地址',
						sort: true,
						width: 250,
						align: 'center',
					}, {
						field: 'jobTitle',
						title: '标题',
						sort: true,
						width: 250,
						align: 'center',
					}, {
						field: 'status',
						title: '状态',
						sort: true,
						width: 100,
						align: 'center',
					}, {
						field: 'effectiveDate',
						title: '失效日期',
						sort: true,
						width: 200,
						align: 'center'
					}, {
						field: 'createDateTime',
						title: '创建时间',
						sort: true,
						width: 200,
						align: 'center'
					}, {
						title: '操作',
						sort: true,
						width: 180,
						fixed: 'right',
						align: 'center',
						toolbar: '#bar_operation'
					}]
				],
				id: 'recReload',
				page: true
			});

			var $ = layui.$,
				active = {
					reload: function() {
						var companyName = $('#companyName').val();
						var area = $('#area').val();
						var status = $('#status').val();
						var startDate = $('#startDate').val();
						var endDate = $('#endDate').val();
						table.reload('recReload', {
							where: {
								companyName: companyName,
								area: area,
								status: status,
								startDate: startDate,
								endDate: endDate,
							}
						});
					}
				};
			//监听提交
			form.on('submit(search)', function(data) {
				data = data.field;
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
				return false;
			});

			//监听工具条
			table.on('tool(rec)', function(obj) {
				var data = obj.data;
				var _this = this;				
				if(obj.event == 'del') {
					layer.confirm('确定要删除吗？', function(index) {
						  $.ajax({  
						          type : "post",  
						          url :  "/recruitment/remove",  		         					    
						          data : {'rid':data.rid},		         
						          success : function(data){ 						          		         	
						          		layer.msg(data.msg,{time:800});								        	 
						          },						         
						    });
						layer.close(index);
						setTimeout(function() {
							var type = $(_this).data('type');
							active[type] ? active[type]() : '';
						}, 1000);

					});
				} else if(obj.event == 'edit') {						
					layer.open({
						type: 2,
						content: '/sharejob/recruitmentEdit?rid=' + data.rid,
						area: ['80%', '90%'] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
					});
				}
			});

		});

		function closeAll() {
			layer.closeAll('iframe');
			$("#search").click();
		}
	</script>

	<script type="text/html" id="bar_operation">
		<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" data-type="reload">删除</a>
	</script>

</html>