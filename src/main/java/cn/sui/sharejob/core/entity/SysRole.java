package cn.sui.sharejob.core.entity;

import java.io.Serializable;
import java.util.List;

public class SysRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1592694075335471481L;
	private String id; // 编号
	private String name; // 角色标识程序中判断使用,如"admin",这个是唯一的:
	private String description; // 角色描述,UI界面显示使用
	private Boolean available = Boolean.FALSE; // 是否可用,如果不可用将不会添加给用户

	private List<SysPermission> permissiones;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public List<SysPermission> getPermissiones() {
		return permissiones;
	}

	public void setPermissiones(List<SysPermission> permissiones) {
		this.permissiones = permissiones;
	}

}