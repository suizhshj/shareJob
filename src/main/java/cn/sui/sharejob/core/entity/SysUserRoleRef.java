package cn.sui.sharejob.core.entity;

/**
 * 
 * <p>
 * Title: UserRoleRef
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月23日
 *
 */
public class SysUserRoleRef {
	private String id;
	private String sysuserId;
	private String roleId;

	public SysUserRoleRef() {

	}

	public SysUserRoleRef(String sysuserId, String roleId) {
		this.sysuserId = sysuserId;
		this.roleId = roleId;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSysuserId() {
		return sysuserId;
	}

	public void setSysuserId(String sysuserId) {
		this.sysuserId = sysuserId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
