package cn.sui.sharejob.core.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 
 * <p>
 * Title: SysUser
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月25日
 *
 */
public class SysUser implements Serializable {

	private static final long serialVersionUID = -1073638492493589965L;
	private String id; // id
	// @Pattern(regexp = "^1[3|4|5|7|8][0-9]\\d{4,8}$", message = "手机号格式错误")
	@NotBlank(message = "用户名不能为空")
	private String userName; // 手机号
	@NotBlank(message = "密码不可能为空")
	private String password; // 登录密码
	private String name; // 姓名
	private Date createDateTime; // 创建时间
	private Date lastUpdateDateTime; // 最后更新时间
	private SysRole role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Date lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public SysRole getRole() {
		return role;
	}

	public void setRole(SysRole role) {
		this.role = role;
	}

}
