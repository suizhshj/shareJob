package cn.sui.sharejob.core.entity;

/**
 * 
 * <p>
 * Title: PermissionRoleRef
 * </p>
 * <p>
 * Description:角色权限关系实体
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月23日
 *
 */
public class SysPermissionRoleRef {

	private String id;
	private String permissionId;
	private String roleId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
