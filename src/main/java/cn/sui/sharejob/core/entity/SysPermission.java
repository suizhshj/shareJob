package cn.sui.sharejob.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * <p>
 * Title: SysPermission
 * </p>
 * <p>
 * Description: 权限实体
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月23日
 *
 */
public class SysPermission implements Serializable {

	private static final long serialVersionUID = 1756570115009403633L;
	private String id;// 主键.
	private String name;// 名称.
	private String resourceType;// 资源类型，[menu|button]
	private String url;// 资源路径.
	private String permission; // 权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view
	private String parentId; // 父编号
	private String parentIds; // 父编号列表
	private String sort; // 排序
	private Date createDateTime; // 创建时间
	private Date lastUpdateDateTime; // 最后更新时间
	private List<SysPermission> subSysPermissiones; // 子菜单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getParentId() {
		return parentId;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getLastUpdateDateTime() {
		return lastUpdateDateTime;
	}

	public void setLastUpdateDateTime(Date lastUpdateDateTime) {
		this.lastUpdateDateTime = lastUpdateDateTime;
	}

	public List<SysPermission> getSubSysPermissiones() {
		return subSysPermissiones;
	}

	public void setSubSysPermissiones(List<SysPermission> subSysPermissiones) {
		this.subSysPermissiones = subSysPermissiones;
	}

}