package cn.sui.sharejob.core.service;

import cn.sui.sharejob.core.entity.SysUser;

public interface SysUserService {

	/**
	 * 根据手机查找
	 * 
	 * @param username
	 * @return
	 */
	public SysUser getByUserName(String username);

	/**
	 * 用户注册
	 * 
	 * @param sysuser
	 */
	public void create(SysUser sysuser);

	/**
	 * 用户基本信息修改
	 * 
	 * @param sysUser
	 */
	public void modify(SysUser sysUser);

	/**
	 * 
	 * @param sysuser
	 * 
	 */
	public void modifyPwd(SysUser sysuser, String oldPwd);

}
