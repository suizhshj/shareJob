package cn.sui.sharejob.core.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import cn.sui.sharejob.core.service.SysPermissionRoleRefService;

@Service
public class SysPermissionRoleRefImpl implements SysPermissionRoleRefService {

	public static void main(String[] args) {
		String phone = "15013664555";
		String regex = "^1[3|4|5|7|8][0-9]\\d{4,8}$";
		if (phone.length() != 11) {
			System.out.println("手机号应为11位数");
		} else {
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(phone);
			boolean isMatch = m.matches();
			if (isMatch) {
				System.out.println("您的手机号" + phone + "是正确格式@——@");
			} else {
				System.out.println("您的手机号" + phone + "是错误格式！！！");
			}
		}
	}

}
