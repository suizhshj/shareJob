package cn.sui.sharejob.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sui.sharejob.core.dao.SysPermissionDao;
import cn.sui.sharejob.core.entity.SysPermission;
import cn.sui.sharejob.core.service.SysPermissionService;

@Service
public class SysPermissionServiceImple implements SysPermissionService {

	@Autowired
	private SysPermissionDao sysPermissionDao;

	@Override
	public List<SysPermission> findAll() {
		// TODO Auto-generated method stub
		return sysPermissionDao.findAll();
	}

}
