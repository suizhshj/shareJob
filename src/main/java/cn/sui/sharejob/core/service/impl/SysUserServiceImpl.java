package cn.sui.sharejob.core.service.impl;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.sui.sharejob.core.dao.SysPermissionDao;
import cn.sui.sharejob.core.dao.SysRoleDao;
import cn.sui.sharejob.core.dao.SysUserDao;
import cn.sui.sharejob.core.dao.SysUserRoleRefDao;
import cn.sui.sharejob.core.entity.SysPermission;
import cn.sui.sharejob.core.entity.SysRole;
import cn.sui.sharejob.core.entity.SysUser;
import cn.sui.sharejob.core.entity.SysUserRoleRef;
import cn.sui.sharejob.core.service.SysUserService;

@Service
public class SysUserServiceImpl implements SysUserService {

	@Autowired
	private SysUserDao sysUserDao;

	@Autowired
	private SysRoleDao sysRoleDao;

	@Autowired
	private SysPermissionDao sysPermissionDao;

	@Autowired
	private SysUserRoleRefDao sysUserRoleRefDao;

	@Value("${cn.tutu.blockchain.newuser.roleid}")
	private String newUserRole;

	@Override
	public SysUser getByUserName(String userName) {
		SysUser sysuser = sysUserDao.findByUserName(userName);
		SysRole role = null;
		if (null != sysuser) {
			role = sysRoleDao.findByUserId(sysuser.getId());
			sysuser.setRole(role);
		}

		if (null != role) {
			List<SysPermission> permissiones = sysPermissionDao.findByRoleId(role.getId());
			role.setPermissiones(permissiones);
		}
		return sysuser;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 30000, rollbackFor = Exception.class)
	public void create(SysUser sysuser) {

		// 校验手机号是否已注册
		SysUser user = sysUserDao.findByUserName(sysuser.getUserName());
		if (null != user) {
			throw new RuntimeException("手机号已注册");
		}
		// 校验验证码是否正确
		// TODO
		// 调用EDH接口获取edhId
		// TODO
		// 密码加密
		Object result = new SimpleHash("MD5", sysuser.getPassword(),
				ByteSource.Util.bytes(sysuser.getUserName().getBytes()), 5);

		sysuser.setPassword(result.toString());

		sysUserDao.insert(sysuser);

		// 设置新用户角色
		SysUserRoleRef sysUserRoleRef = new SysUserRoleRef(sysuser.getId(), newUserRole);
		sysUserRoleRefDao.insert(sysUserRoleRef);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 30000, rollbackFor = Exception.class)
	public void modify(SysUser sysUser) {
		sysUserDao.update(sysUser);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 30000, rollbackFor = Exception.class)
	public void modifyPwd(SysUser sysuser, String oldPwd) {

		Session session = SecurityUtils.getSubject().getSession();
		String code = (String) session.getAttribute(sysuser.getUserName());

		String hashOldPwd = new SimpleHash("MD5", oldPwd, ByteSource.Util.bytes(sysuser.getUserName().getBytes()), 5)
				+ "";
		SysUser user = sysUserDao.findByUserName(sysuser.getUserName());

		if (!hashOldPwd.equals(user.getPassword())) {
			throw new RuntimeException("旧密码错误");
		}

		// 密码加密
		Object result = new SimpleHash("MD5", sysuser.getPassword(),
				ByteSource.Util.bytes(sysuser.getUserName().getBytes()), 5);

		sysuser.setPassword(result.toString());
		sysUserDao.updatePwd(sysuser);
	}

}
