package cn.sui.sharejob.core.service;

import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.core.entity.Recruitment;

public interface RecruitmentService {

	PageInfo<Recruitment> getByModel(Recruitment recruitment, Page page);

	void create(Recruitment recruitment);

	Recruitment getById(String id);

	void modify(Recruitment recruitment);

	void remove(String id);

	int modifyStatusByOverdue();

}
