package cn.sui.sharejob.core.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.core.entity.Comment;

public interface CommentService {

	List<Comment> getByRecId(String recId);

	void create(Comment comment);

	PageInfo<Comment> getByRecTitle(String title, Page page);

}
