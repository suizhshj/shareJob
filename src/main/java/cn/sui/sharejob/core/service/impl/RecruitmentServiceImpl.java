package cn.sui.sharejob.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.core.dao.RecruitmentDao;
import cn.sui.sharejob.core.entity.Recruitment;
import cn.sui.sharejob.core.service.RecruitmentService;

@Service
@Transactional
public class RecruitmentServiceImpl implements RecruitmentService {

	@Autowired
	private RecruitmentDao recruitmentDao;

	@Override
	public PageInfo<Recruitment> getByModel(Recruitment recruitment, Page page) {
		PageHelper.startPage(page.getPage(), page.getLimit());
		List<Recruitment> list = recruitmentDao.findByModel(recruitment);
		PageInfo<Recruitment> pageInfo = new PageInfo<Recruitment>(list);
		return pageInfo;

	}

	@Override
	public void create(Recruitment recruitment) {
		recruitmentDao.insert(recruitment);
	}

	@Override
	public Recruitment getById(String id) {
		return recruitmentDao.findById(id);
	}

	@Override
	public void modify(Recruitment recruitment) {
		recruitmentDao.update(recruitment);
	}

	@Override
	public void remove(String id) {
		recruitmentDao.delete(id);
	}

	@Override
	public int modifyStatusByOverdue() {
		return recruitmentDao.updateStatusByOverdue();
	}

}
