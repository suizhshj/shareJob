package cn.sui.sharejob.core.service;

import java.util.List;

import cn.sui.sharejob.core.entity.SysPermission;

public interface SysPermissionService {

	List<SysPermission> findAll();

}
