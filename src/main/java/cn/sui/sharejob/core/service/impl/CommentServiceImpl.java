package cn.sui.sharejob.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.core.dao.CommentDao;
import cn.sui.sharejob.core.entity.Comment;
import cn.sui.sharejob.core.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDao commentDao;
	@Override
	public List<Comment> getByRecId(String recId) {
		List<Comment> list = commentDao.findByRecId(recId);
		return list;
	}
	@Override
	public void create(Comment comment) {
		commentDao.insert(comment);
		
	}
	@Override
	public PageInfo<Comment> getByRecTitle(String title, Page page) {
		PageHelper.startPage(page.getPage(), page.getLimit());
		List<Comment> list = commentDao.findByRecTitle(title);
		PageInfo<Comment> pageInfo = new PageInfo<Comment>(list);
		return pageInfo;
	}

}
