package cn.sui.sharejob.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.SysPermission;

@Mapper
public interface SysPermissionDao extends BaseDao<SysPermission> {

	List<SysPermission> findByRoleId(String roleId);

}
