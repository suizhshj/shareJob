package cn.sui.sharejob.core.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.SysUserRoleRef;

@Mapper
public interface SysUserRoleRefDao extends BaseDao<SysUserRoleRef> {

}
