package cn.sui.sharejob.core.dao;

import java.util.List;

/**
 * 
 * <p>
 * Title: BaseDao
 * </p>
 * <p>
 * Description: CURD接口
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月23日
 *
 */
public interface BaseDao<T> {

	/**
	 * 保存实体
	 * 
	 * @param entity
	 */
	int insert(T entity);

	/**
	 * 删除实体
	 * 
	 * @param id
	 */
	void delete(String id);

	/**
	 * 更新实体
	 * 
	 * @param entity
	 */
	void update(T entity);

	/**
	 * 查询实体，如果id为null，则返回null，并不会抛异常。
	 * 
	 * @param id
	 * @return
	 */
	T findById(String id);

	/**
	 * 根据条件查询
	 * 
	 * @param model
	 *            实体对象
	 * @return
	 */
	List<T> findByModel(T model);

	/**
	 * 查询所有
	 * 
	 * @return
	 */
	List<T> findAll();

}
