package cn.sui.sharejob.core.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.SysPermissionRoleRef;
import cn.sui.sharejob.core.entity.SysRole;

@Mapper
public interface SysRoleDao extends BaseDao<SysPermissionRoleRef> {

	/**
	 * 根据用户ID查找角色
	 * 
	 * @param id
	 * @return
	 */
	SysRole findByUserId(String userId);

}
