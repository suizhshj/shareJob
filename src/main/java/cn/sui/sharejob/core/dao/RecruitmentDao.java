package cn.sui.sharejob.core.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.Recruitment;

@Mapper
public interface RecruitmentDao extends BaseDao<Recruitment> {

	int updateStatusByOverdue();

}
