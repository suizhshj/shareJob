package cn.sui.sharejob.core.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.SysPermissionRoleRef;

@Mapper
public interface SysPermissionRoleRefDao extends BaseDao<SysPermissionRoleRef> {

}
