package cn.sui.sharejob.core.dao;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.SysUser;

@Mapper
public interface SysUserDao extends BaseDao<SysUser> {

	SysUser findByUserName(String phone);

	void updatePwd(SysUser sysuser);

	void updatePwdByPhone(SysUser sysuser);

}
