package cn.sui.sharejob.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.sui.sharejob.core.entity.Comment;

@Mapper
public interface CommentDao extends BaseDao<Comment>{

	List<Comment> findByRecId(String recId);

	List<Comment> findByRecTitle(String title);

}
