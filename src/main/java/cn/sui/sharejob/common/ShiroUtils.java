package cn.sui.sharejob.common;

import org.apache.shiro.SecurityUtils;

import cn.sui.sharejob.core.entity.SysUser;

public class ShiroUtils {

	public static SysUser getLoginUser() {
		return (SysUser) SecurityUtils.getSubject().getPrincipal();
	}

}
