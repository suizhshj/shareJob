package cn.sui.sharejob.common;

/**
 * 
 * @ClassName: MsgEnum
 * @Description: TODO(错误枚举类)
 * @author 眭善峰
 * @date 2017年10月28日 下午12:51:28
 *
 */
public enum MsgEnum {

	// layui返回数据集合列表
	LAYUI_SUCCESS("0", "SUCCESS"),

	SUCCESS("SUCCESS", "操作成功"),

	EXISTS("EXISTS", "已存在"),

	ERROR("SYSTEM_ERROR", "系统错误，请稍后再试"),

	EXCEPTION("SYSTEM_EXCEPTION", "系统异常，请稍后再试"),

	PARAM_NULL("PARAM_NULL", "参数空异常，请稍后再试"),

	PARAM_ERROR("PARAM_ERROR", "参数错误，请稍后再试"),

	PARAM_VALID("PARAM_VALID", "参数验证异常，请稍后再试"),

	OBJ_NULL("OBJ_NULL", "空对象异常"),

	MSG_EXCEPTION("MSG_EXCEPTION", "短信发送失败"),

	NO_LOGIN("NO_LOGIN", "未登录"),

	NO_AUTHEN("NO_AUTHEN", "没有权限");

	String code;

	String message;

	Object data;

	MsgEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * 获取错误码
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 获取错误信息
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
