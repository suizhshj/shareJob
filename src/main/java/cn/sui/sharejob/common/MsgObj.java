package cn.sui.sharejob.common;

public class MsgObj {

	private MsgObj() {
	}

	/**
	 * @Fields:返回码
	 */
	private String code;

	/**
	 * @Fields:返回消息
	 */
	private String msg;

	/**
	 * @Fields:返回数据
	 */
	private Object data;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public MsgObj(String code, String message, Object data) {
		super();
		this.code = code;
		this.msg = message;
		this.data = data;
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	public static MsgObj buildSuccessMsg() {
		return buildSuccessMsg(MsgEnum.SUCCESS.getCode(), MsgEnum.SUCCESS.getMessage(), null);
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	public static MsgObj buildSuccessMsg(String message) {
		return buildSuccessMsg(MsgEnum.SUCCESS.getCode(), message, null);
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	public static MsgObj buildSuccessObj(Object obj) {
		return buildSuccessMsg(MsgEnum.SUCCESS.getCode(), MsgEnum.SUCCESS.getMessage(), obj);
	}

	/**
	 * 返回成功对象
	 * 
	 * @param data
	 * @return
	 */
	public static MsgObj buildSuccessMsg(String code, String message, Object data) {
		MsgObj mo = new MsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		mo.setData(data);
		return mo;
	}

	/**
	 * 返回错误对象
	 * 
	 * @param e
	 * @return
	 */
	public static MsgObj buildErrorMsg(String code, String message) {
		MsgObj mo = new MsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		return mo;
	}

	/**
	 * 返回错误对象
	 * 
	 * @param e
	 * @return
	 */
	public static MsgObj buildErrorMsg() {
		return buildErrorMsg(MsgEnum.ERROR.getCode(), MsgEnum.ERROR.getMessage());
	}

	/**
	 * 返回异常
	 * 
	 * @return
	 */
	public static MsgObj buildExceptionMsg(Exception e) {
		return buildExceptionMsg(MsgEnum.EXCEPTION.getCode(), MsgEnum.EXCEPTION.getMessage(), e);
	}

	/**
	 * 返回异常信息
	 * 
	 * @param code
	 * @param message
	 * @return
	 */
	public static MsgObj buildExceptionMsg(String message, Exception e) {
		return buildExceptionMsg(MsgEnum.EXCEPTION.getCode(), message, e);
	}

	/**
	 * 返回异常信息
	 * 
	 * @param code
	 * @param message
	 * @return
	 */
	public static MsgObj buildExceptionMsg(String message) {
		return buildExceptionMsg(MsgEnum.EXCEPTION.getCode(), message, null);
	}

	/**
	 * 返回异常信息
	 * 
	 * @param code
	 * @param message
	 * @return
	 */
	public static MsgObj buildExceptionMsg(String code, String message) {
		return buildExceptionMsg(code, message, null);
	}

	/**
	 * 返回异常信息
	 * 
	 * @param code
	 * @param message
	 * @return
	 */
	private static MsgObj buildExceptionMsg(String code, String message, Exception e) {
		MsgObj mo = new MsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		mo.setData(e);
		return mo;
	}

	/**
	 * 返回成功信息
	 * 
	 * @param object
	 * @return
	 */
	public static MsgObj buildSuccessMsg(Object object) {
		MsgObj mo = new MsgObj();
		mo.setCode(MsgEnum.SUCCESS.getCode());
		mo.setMsg(MsgEnum.SUCCESS.getMessage());
		mo.setData(object);
		return mo;
	}

}
