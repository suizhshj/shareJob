package cn.sui.sharejob.common;

public class ResMsgObj {

	// 0 正常
	private String code;
	// 提示信息
	private String msg;
	// 数量
	private long count;
	// 列表数据对象
	private Object data;

	public ResMsgObj() {
	}

	public ResMsgObj(String code, String msg, long count, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.count = count;
		this.data = data;
	}

	public ResMsgObj(String code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	/**
	 * 返回成功列表数据
	 * 
	 * @param count
	 *            返回集合列表的对象数量
	 * @param obj
	 *            返回的集合对象
	 * @return
	 */
	public static ResMsgObj buildSuccessObj(long count, Object obj) {
		return buildSuccessMsg(ResMsgEnum.SUCCESS.getCode(), ResMsgEnum.SUCCESS.getMessage(), count, obj);
	}

	public static ResMsgObj buildSuccessObj(Object obj) {
		return buildSuccessMsg(ResMsgEnum.SUCCESS.getCode(), ResMsgEnum.SUCCESS.getMessage(), obj);
	}

	/**
	 * 返回处理成功
	 * 
	 * @return
	 */
	public static ResMsgObj buildSuccessObj() {
		return buildSuccessMsg(ResMsgEnum.SUCCESS.getCode(), ResMsgEnum.SUCCESS.getMessage(), 0, null);
	}

	/**
	 * 返回成功对象
	 * 
	 * @param data
	 * @return
	 */
	public static ResMsgObj buildSuccessMsg(String code, String message, long count, Object data) {
		ResMsgObj mo = new ResMsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		mo.setData(data);
		mo.setCount(count);
		return mo;
	}

	public static ResMsgObj buildSuccessMsg(String code, String message, Object data) {
		ResMsgObj mo = new ResMsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		mo.setData(data);
		return mo;
	}

	/**
	 * 返回异常对象
	 * 
	 * @param e
	 * @return
	 */
	public static ResMsgObj buildExceptionMsg(Exception e) {
		return buildExceptionMsg(ResMsgEnum.SYSTEM_EXCEPTION.getCode(), ResMsgEnum.SYSTEM_EXCEPTION.getMessage());
	}

	/**
	 * 返回异常消息
	 * 
	 * @param e
	 * @return
	 */
	public static ResMsgObj buildExceptionMsg(String message) {
		return buildExceptionMsg(ResMsgEnum.SYSTEM_EXCEPTION.getCode(), message);
	}

	private static ResMsgObj buildExceptionMsg(String code, String message) {
		ResMsgObj mo = new ResMsgObj();
		mo.setCode(code);
		mo.setMsg(message);
		mo.setData(null);
		mo.setCount(0);
		return mo;

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
