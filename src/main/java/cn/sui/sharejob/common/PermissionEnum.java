package cn.sui.sharejob.common;

public enum PermissionEnum {

	INIT_MENU("0", "一级菜单");

	PermissionEnum(String code, String explain) {
		this.code = code;
		this.explain = explain;

	}

	String code;
	String explain;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

}
