package cn.sui.sharejob.common;

/**
 * 
 * @ClassName: MsgEnum
 * @Description: TODO(错误枚举类)
 * @author 眭善峰
 * @date 2017年10月28日 下午12:51:28
 *
 */
public enum ResMsgEnum {

	SUCCESS("0", "SUCCESS"), SYSTEM_EXCEPTION("1", "系统异常,请稍后再试");
	String code;

	String message;

	ResMsgEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
