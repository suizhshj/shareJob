package cn.sui.sharejob.common;

/**
 * 
 * @ClassName: Page
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author
 * @date 2017年10月28日 下午4:15:52
 *
 */
public class Page {

	/**
	 * 
	 */

	// layUi名称
	private Integer limit; // 当前页码
	private Integer page;// 分页大小

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
