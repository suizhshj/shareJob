package cn.sui.sharejob.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.sui.sharejob.admin.SysuserController;
import cn.sui.sharejob.core.service.RecruitmentService;

@Component
public class ScheduledTask {

	private static Logger LOGGER = LoggerFactory.getLogger(SysuserController.class);

	@Autowired
	private RecruitmentService recruitmentService;

	/**
	 * 过期的招聘信息更状态为失效
	 */
	@Scheduled(cron = "* * 1 * * *")
	public void task1() {
		LOGGER.info("启动定时任务：开始修改过期招聘信息的状态");
		int i = recruitmentService.modifyStatusByOverdue();
		LOGGER.info("成功修改过期招聘信息状态{}条...", i);
	}
}
