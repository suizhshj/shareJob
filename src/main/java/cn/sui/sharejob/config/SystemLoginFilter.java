package cn.sui.sharejob.config;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.springframework.beans.factory.annotation.Autowired;

import cn.sui.sharejob.core.dao.SysUserDao;
import cn.sui.sharejob.core.entity.SysUser;

public class SystemLoginFilter extends LogoutFilter {

	@Autowired
	private SysUserDao sysuserDao;

	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		Subject subject = getSubject(request, response);
		SysUser user = (SysUser) subject.getPrincipal();
		if (null != user) {
			// sysuserDao.updateLoginStatus(user);
			System.out.println("修改登录状态");
		}
		return super.preHandle(request, response);
	}

}
