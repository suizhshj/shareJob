package cn.sui.sharejob.admin;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.sui.sharejob.core.entity.Recruitment;
import cn.sui.sharejob.core.entity.SysUser;
import cn.sui.sharejob.core.service.RecruitmentService;

@Controller
@RequestMapping("/sharejob")
public class HomeController {

	@Autowired
	private RecruitmentService recruitmentService;

	/**
	 * 首页
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/index")
	public String index(Model model) {
		Subject subject = SecurityUtils.getSubject();
		SysUser sysUser = (SysUser) subject.getPrincipal();
		model.addAttribute("role", sysUser.getRole());
		model.addAttribute("sysuser", sysUser);
		return "/home/index";
	}

	/**
	 * 欢迎页
	 * 
	 * @return
	 */
	@RequestMapping("/welcome")
	public String welcome() {
		return "/home/welcome";
	}

	/**
	 * 登录页面
	 * 
	 * @return
	 */
	@RequestMapping("/login")
	public String login() {
		return "/home/login";
	}

	@RequestMapping("/recruitmentMain")
	public String RecruitmentMain() {
		return "/recruitment/recruitment-list";
	}

	@RequestMapping("/recruitmentAdd")
	public String RecruitmentAdd() {
		return "/recruitment/recruitment-add";
	}

	@RequestMapping("/recruitmentEdit")
	public String RecruitmentEdit(String rid, Model model) {
		Recruitment recruitment = recruitmentService.getById(rid);
		model.addAttribute("recruitment", recruitment);
		return "/recruitment/recruitment-edit";
	}

}
