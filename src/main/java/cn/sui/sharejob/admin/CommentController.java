package cn.sui.sharejob.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.common.ResMsgObj;
import cn.sui.sharejob.core.entity.Comment;
import cn.sui.sharejob.core.service.CommentService;

@RestController
public class CommentController {
	
	private CommentService commentService;
	

	@RequestMapping("/getGrid")
	public ResMsgObj getGrid(String title, Page page) {
		PageInfo<Comment> byModel = commentService.getByRecTitle(title, page);
		return ResMsgObj.buildSuccessObj(byModel.getTotal(), byModel.getList());

	}
	

}
