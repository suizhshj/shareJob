package cn.sui.sharejob.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.MsgObj;
import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.common.ResMsgObj;
import cn.sui.sharejob.core.entity.Recruitment;
import cn.sui.sharejob.core.service.RecruitmentService;

@RestController
@RequestMapping("/recruitment")
public class RecruitmentController {

	@Autowired
	private RecruitmentService recruitmentService;

	@RequestMapping("/getGrid")
	public ResMsgObj getRecruitment(Recruitment recruitment, Page page) {
		System.out.println();
		PageInfo<Recruitment> byModel = recruitmentService.getByModel(recruitment, page);
		return ResMsgObj.buildSuccessObj(byModel.getTotal(), byModel.getList());

	}

	@RequestMapping("/create")
	public MsgObj create(@Valid Recruitment recruitment) {
		recruitmentService.create(recruitment);
		return MsgObj.buildSuccessMsg();

	}

	@RequestMapping("/modify")
	public MsgObj modify(@Valid Recruitment recruitment) {
		recruitmentService.modify(recruitment);
		return MsgObj.buildSuccessMsg();

	}

	@RequestMapping("/remove")
	public MsgObj remove(String rid) {
		recruitmentService.remove(rid);
		return MsgObj.buildSuccessMsg();

	}

	@RequestMapping("/getById")
	public MsgObj getByRid(String id) {
		Recruitment recruitment = recruitmentService.getById(id);
		return MsgObj.buildSuccessMsg(recruitment);
	}

}
