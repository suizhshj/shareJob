package cn.sui.sharejob.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import cn.sui.sharejob.core.entity.SysPermission;
import cn.sui.sharejob.core.service.SysPermissionService;

@RestController
public class SysPermissionController {
	@Autowired
	private SysPermissionService sysPermissionService;

	public List<SysPermission> getPermissionList() {
		return sysPermissionService.findAll();
	}

}
