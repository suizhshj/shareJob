package cn.sui.sharejob.admin;

import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.sui.sharejob.common.MsgObj;
import cn.sui.sharejob.core.entity.SysUser;

@RestController
@Validated
@RequestMapping("/sysuser")
public class SysuserController {

	private static Logger LOGGER = LoggerFactory.getLogger(SysuserController.class);

	@RequestMapping("/ajaxLogin")
	public MsgObj ajaxLogin(String username, String password) {
		LOGGER.info("登录:username[{}],password[{}]", username, password);
		MsgObj msg = MsgObj.buildSuccessMsg();

		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		Subject subject = SecurityUtils.getSubject();
		subject.login(token);
		// 登录成功修改登录状态
		SysUser sysuser = (SysUser) subject.getPrincipal();
		// sysuser.setLoginStatus(Boolean.TRUE);
		// sysUserService.updateLoginStatus(sysuser);
		MsgObj.buildSuccessObj(sysuser);
		return msg;
	}

	@RequestMapping("/modify")
	@RequiresPermissions("sysuser[modify]")
	public MsgObj modify(SysUser sysUser) {
		LOGGER.info("用户修改:参数[{}]", sysUser.toString());
		// sysUserService.update(sysUser);
		return MsgObj.buildSuccessMsg();
	}

	@RequestMapping("/modifyPwd")
	@RequiresPermissions("sysuser[modifyPwd]")
	public MsgObj modifyPwd(@Valid SysUser sysuser, @NotBlank(message = "原密码不可能为空") String oldPwd) {
		LOGGER.info("用户修改密码:用户[{}],新密码[{}]", sysuser.getUserName(), sysuser.getPassword());
		// sysUserService.updatePwd(sysuser, oldPwd);
		return MsgObj.buildSuccessMsg();
	}

	@RequestMapping("/resetPwd")
	public MsgObj resetPwd(@Valid SysUser sysuser) {
		LOGGER.info("用户重置密码:用户[{}],新密码[{}]", sysuser.getUserName(), sysuser.getPassword());
		// sysUserService.resetPwd(sysuser);
		return MsgObj.buildSuccessMsg();
	}

}
