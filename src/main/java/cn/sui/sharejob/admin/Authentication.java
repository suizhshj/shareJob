package cn.sui.sharejob.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.sui.sharejob.common.MsgEnum;
import cn.sui.sharejob.common.MsgObj;

/**
 * 
 * <p>
 * Title: Authentication
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author 眭善峰
 * @date 2018年4月23日
 *
 */
@RestController
public class Authentication {

	/**
	 * 
	 * @return
	 */
	@RequestMapping("/unlogin")
	public MsgObj unlogin() {
		return MsgObj.buildExceptionMsg(MsgEnum.NO_LOGIN.getCode(), MsgEnum.NO_LOGIN.getMessage());

	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping("/unauthorized")
	public MsgObj unauthorized() {
		return MsgObj.buildExceptionMsg(MsgEnum.NO_AUTHEN.getCode(), MsgEnum.NO_AUTHEN.getMessage());
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping("/kickout")
	public MsgObj kickout() {
		return MsgObj.buildExceptionMsg("被踢出!");
	}

}
