package cn.sui.sharejob.protal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import cn.sui.sharejob.common.Page;
import cn.sui.sharejob.common.ResMsgObj;
import cn.sui.sharejob.core.entity.Recruitment;
import cn.sui.sharejob.core.service.RecruitmentService;

@RestController
@RequestMapping("/recProtal")
public class RecruitmentProtal {
	@Autowired
	private RecruitmentService recruitmentService;

	@RequestMapping("/getGrid")
	public ResMsgObj getRecruitment(Recruitment recruitment, Page page) {
		System.out.println();
		PageInfo<Recruitment> byModel = recruitmentService.getByModel(recruitment, page);
		System.out.println(byModel.getList().size() + "=================================");
		return ResMsgObj.buildSuccessObj(byModel);

	}
}
