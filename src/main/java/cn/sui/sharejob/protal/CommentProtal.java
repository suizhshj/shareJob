package cn.sui.sharejob.protal;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.sui.sharejob.common.MsgObj;
import cn.sui.sharejob.core.entity.Comment;
import cn.sui.sharejob.core.service.CommentService;

@RestController
@RequestMapping("/commentProtal")
public class CommentProtal {
	@Autowired
	private CommentService commentService;
	
	@RequestMapping("/getGrid")
	public List<Comment> getCommentList(String recId) {
		List<Comment> commentList = commentService.getByRecId(recId);
		return commentList;

	} 
	
	@RequestMapping("/create")
	public MsgObj create(@Valid Comment comment) {
		commentService.create(comment);
		return MsgObj.buildSuccessMsg();

	} 

}
